package ru.t1.akolobov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.service.IProjectService;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.akolobov.tm.exception.field.*;
import ru.t1.akolobov.tm.model.Project;

import java.util.List;
import java.util.Optional;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository>
        implements IProjectService {

    public ProjectService(@NotNull IProjectRepository repository) {
        super(repository);
    }

    @Override
    @NotNull
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @Override
    @NotNull
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        return project;
    }

    @Override
    @NotNull
    public Project changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setStatus(status);
        return project;
    }

    @Override
    @Nullable
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return add(userId, new Project(name));
    }

    @Override
    @Nullable
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return add(userId, new Project(name));
        return add(userId, new Project(name, description));
    }

    @Override
    @Nullable
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (status == null) return add(userId, new Project(name));
        return add(userId, new Project(name, status));
    }

    @Override
    @NotNull
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @NotNull
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Project project = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(ProjectNotFoundException::new);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
