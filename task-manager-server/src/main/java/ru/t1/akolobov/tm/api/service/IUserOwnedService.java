package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @Nullable
    M add(@NotNull String userId, @NotNull M model);

    void clear(@Nullable String userId);

    boolean existById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<? super M> comparator);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Integer getSize(@NotNull String userId);

    @Nullable
    M remove(@NotNull String userId, @NotNull M model);

    @NotNull
    M removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    M removeByIndex(@Nullable String userId, @Nullable Integer index);

}
