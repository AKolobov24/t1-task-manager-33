package ru.t1.akolobov.tm.api.repository;

import ru.t1.akolobov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
